<?php

namespace App\Http\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AttendeeResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [

            'id' => $this->id,
            'name'=> $this->name,
            'description'=> $this->description,
            'user'=>new UserResource($this->whenLoaded('user')),
            'attendes'=> AttendeeResource::collection($this->whenLoaded('attendee')),


        ];
    }
}
